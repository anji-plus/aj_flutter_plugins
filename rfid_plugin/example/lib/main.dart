import 'dart:developer';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:rfid_plugin/rfid_plugin.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _info = 'Unknown';
  RfidPlugin plugin = new RfidPlugin();

  @override
  void initState() {
    super.initState();
    plugin.getMessageClickCallback((result) {
      _info = result;
      setState(() {});
    });
    rfidConnect();
    getRfidInfo();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> rfidConnect() async {
    try {
      await RfidPlugin.rfidConnect();
    } on PlatformException {}
  }

  Future<void> rfidDisConnect() async {
    try {
      await RfidPlugin.rfidDisConnect();
    } on PlatformException {}
  }

  Future<void> getRfidInfo() async {
    RfidPlugin.getRfidInfo(onSucceed: (success) {
      _info = success.toString();
      log(success.toString());
      setState(() {});
    }, onFailed: (failed) {
      _info = failed.toString();
      log(failed.toString());
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Running on: $_info\n'),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    rfidDisConnect();
  }
}
