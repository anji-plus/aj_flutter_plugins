package com.anji.plus.rfid_plugin;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import com.honeywell.rfidservice.RfidManager;
import com.honeywell.rfidservice.TriggerMode;
import com.honeywell.rfidservice.rfid.TagReadData;
import com.honeywell.tools.rfidlib.HONRFIDLib;
import com.honeywell.tools.rfidlib.IRfidCallBack;

import java.util.HashMap;
import java.util.Map;


/**
 * RfidPlugin
 */
public class RfidPlugin implements FlutterPlugin, MethodCallHandler {
    private MethodChannel channel;
    private HONRFIDLib honrfidLib;
    private Context context;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "rfid_plugin");
        channel.setMethodCallHandler(this);
        context = flutterPluginBinding.getApplicationContext();
        honrfidLib = new HONRFIDLib(context);
        honrfidLib.init();
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("rfidConnect")) {
            rfidConnect();
        } else if (call.method.equals("rfidDisConnect")) {
            rfidDisConnect();
        } else if (call.method.equals("getRfidInfo")) {
            getRfidInfo();
        }
    }

    private void rfidConnect() {
        if (!RfidManager.getInstance(context).isConnected()) {
            Log.e("RFID:", "第一次连接");
            HONRFIDLib.getIntance().setRfidTriggerMode(TriggerMode.BARCODE_SCAN);
            HONRFIDLib.getIntance().setiRfidCallBack(null);
            honrfidLib.setRfidTriggerMode(TriggerMode.RFID);
            honrfidLib.connect();
        } else {
            Log.e("RFID:", "之前已经链接");
        }
    }

    private void rfidDisConnect() {
        if (honrfidLib != null && RfidManager.getInstance(context).isConnected()) {
            HONRFIDLib.getIntance().setiRfidCallBack(null);
            honrfidLib.disconnect();
            honrfidLib.unInit();
            honrfidLib = null;
            Log.e("RFID:", "已经回收!");
        }
    }

    private void getRfidInfo() {
        //todo 设置 天线功率
        HONRFIDLib.getIntance().setAntennaPower(30);
        //todo 设置 fastid 模式
        HONRFIDLib.getIntance().setFastScan(true);

        //todo 大量标签快速单次盘点
        HONRFIDLib.getIntance().setSceneMode(HONRFIDLib.SceneMode.Multi_label_fast_scanning);
        HONRFIDLib.getIntance().setiRfidCallBack(iRfidCallBack);
        HONRFIDLib.getIntance().setRfidTriggerMode(TriggerMode.RFID);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
        //todo 条码 按键扫描模式
        HONRFIDLib.getIntance().setRfidTriggerMode(TriggerMode.BARCODE_SCAN);
        //todo 设置RFID 回调
        HONRFIDLib.getIntance().setiRfidCallBack(null);
        honrfidLib.disconnect();
        honrfidLib.unInit();
        honrfidLib = null;
        Log.e("RFID:", "已经回收!");
    }

    private IRfidCallBack iRfidCallBack = new IRfidCallBack() {
        @Override
        public void onRfidConnected() {

        }

        @Override
        public void onRFIDDisConnect() {

        }

        @Override
        public void onRFIDReady() {

        }

        @Override
        public void onRfidTriggeredChange(Boolean b) {
            if (b) {
                //todo 开始读取标签
                HONRFIDLib.getIntance().startRead();
            } else {
                //todo 停止读取标签
                HONRFIDLib.getIntance().stopRead();
            }
        }

        @Override
        public void onTagData(final TagReadData[] tagReadData) {
            Log.e("标签数据", String.valueOf(tagReadData.length));
            if (tagReadData.length > 0) {
                Log.e("标签 字节", new String(tagReadData[0].getAdditionData()));
                Log.e("标签 ", tagReadData[0].getEpcHexStr());
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (tagReadData.length > 0) {
                        Log.e("标签发送", tagReadData[0].getEpcHexStr());
                        channel.invokeMethod("onMessageClicked", tagReadData[0].getEpcHexStr());
                        HONRFIDLib.getIntance().stopRead();
                        HONRFIDLib.getIntance().playSound();
                    }
                }
            });
        }

        @Override
        public void onScanData(String data) {
        }
    };

    class RfidCallback {

        private Result result;

        public RfidCallback(Result result) {
            this.result = result;
        }

        public void sucess(String contents) {
            result.success(contents);
        }

        public void failed() {
            result.success("识别失败");
        }

    }
}
