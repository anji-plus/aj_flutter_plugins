import 'dart:async';

import 'package:flutter/services.dart';

typedef onMessageClickCallback = void Function(String result);

class RfidPlugin {
  onMessageClickCallback _onMessageClickCallback;
  static const MethodChannel _channel = const MethodChannel('rfid_plugin');
  RfidPlugin() {
    print("AjFlutterAppspPush constructor");
    _channel.setMethodCallHandler(_handlerMethodCallback);
  }

  ///消息点击回调，可接收多个消息
  void getMessageClickCallback(onMessageClickCallback callback) {
    if (callback == null) {
      return;
    }
    this._onMessageClickCallback = callback;
  }

  Future<dynamic> _handlerMethodCallback(MethodCall call) async {
    print("RfidPlugin call.method ${call.method}");
    switch (call.method) {
      case "onMessageClicked":
        print("RfidPlugin call.arguments ${call.arguments}");
        String result = call.arguments;
        print("RfidPlugin result ${result}");
        print("RfidPlugin onMessageClicked ");
        if (_onMessageClickCallback != null) {
          _onMessageClickCallback(result);
        }
        break;
      default:
        break;
    }
  }

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future rfidConnect() async {
    await _channel.invokeMethod('rfidConnect');
  }
  static Future rfidDisConnect() async {
    await _channel.invokeMethod('rfidDisConnect');
  }

  ///获取车牌号
  static Future<String> getRfidInfo({
    void Function(String) onSucceed,
    void Function(String) onFailed,
  }) async {
    try {
      final String result = await _channel.invokeMethod('getRfidInfo');
      onSucceed(result);
    } on PlatformException catch (e) {
      onFailed("识别失败");
    }
  }
}
