#import <Flutter/Flutter.h>

@interface AjFlutterAppspPushPlugin : NSObject<FlutterPlugin>

@property (nonatomic, copy) NSString *host;
@property (nonatomic, copy) NSString *appspAppKey;
@property (nonatomic, copy) NSString *appspSecretKey;
@property (nonatomic, copy) NSString *jpushAppKey;
@property (nonatomic, copy) NSString *jchannel;

@property FlutterMethodChannel *channel;

+ (void)setDataWithDic:(NSDictionary *)dic;
@end
