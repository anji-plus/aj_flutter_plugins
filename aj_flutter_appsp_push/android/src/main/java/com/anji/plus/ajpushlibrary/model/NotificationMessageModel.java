package com.anji.plus.ajpushlibrary.model;

import java.io.Serializable;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 对推送消息做的封装成统一model
 * </p>
 */
public class NotificationMessageModel implements Serializable {
    private String title;
    private String content;
    private int actionType ;//1，无操作，2，点击通知栏消息
    private int brandType;//设备类型，1，华为，2，小米，3，oppo，4，vivo，5，其他
    private String notificationExtras;//自定义参数
    private int messageType;//消息类型  1、极光透传，2、极光通知栏声音，3小米极光点击通知栏跳转页面

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNotificationExtras() {
        return notificationExtras;
    }

    public void setNotificationExtras(String notificationExtras) {
        this.notificationExtras = notificationExtras;
    }

    public int getBrandType() {
        return brandType;
    }

    public void setBrandType(int brandType) {
        this.brandType = brandType;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getActionType() {
        return actionType;
    }

    public void setActionType(int actionType) {
        this.actionType = actionType;
    }

    @Override
    public String toString() {
        return "NotificationMessageModel{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", actionType=" + actionType +
                ", brandType=" + brandType +
                ", notificationExtras='" + notificationExtras + '\'' +
                ", messageType=" + messageType +
                '}';
    }
}
