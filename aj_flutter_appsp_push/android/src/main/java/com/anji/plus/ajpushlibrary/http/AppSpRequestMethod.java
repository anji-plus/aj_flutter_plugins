package com.anji.plus.ajpushlibrary.http;

public enum AppSpRequestMethod {
    POST,
    GET,
    DELETE,
    PUT
}
