package com.anji.plus.ajpushlibrary.vivo;

import android.content.Context;

import com.anji.plus.ajpushlibrary.AppSpLog;
import com.vivo.push.model.UPSNotificationMessage;
import com.vivo.push.sdk.OpenClientPushMessageReceiver;


public class VivoPushReceiver extends OpenClientPushMessageReceiver {
    /**
     * TAG to Log
     */

    @Override
    public void onNotificationMessageClicked(Context context, UPSNotificationMessage msg) {
        String customContentString = msg.getSkipContent();
        String notifyString = "通知点击 msgId " + msg.getMsgId() + " ;customContent=" + customContentString;
        AppSpLog.d(notifyString);
    }

    @Override
    public void onReceiveRegId(Context context, String regId) {
        String responseString = "onReceiveRegId regId = " + regId;
        AppSpLog.d(responseString);
    }
}
